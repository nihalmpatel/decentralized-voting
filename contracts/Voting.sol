pragma solidity ^0.4.17;


/**
 * @title SafeMath
 * @dev Math operations with safety checks that throw on error
 */
library SafeMath {

  /**
  * @dev Multiplies two numbers, throws on overflow.
  */
  function mul(uint256 a, uint256 b) internal pure returns (uint256 c) {
    if (a == 0) {
      return 0;
    }
    c = a * b;
    assert(c / a == b);
    return c;
  }

  /**
  * @dev Integer division of two numbers, truncating the quotient.
  */
  function div(uint256 a, uint256 b) internal pure returns (uint256) {
    // assert(b > 0); // Solidity automatically throws when dividing by 0
    // uint256 c = a / b;
    // assert(a == b * c + a % b); // There is no case in which this doesn't hold
    return a / b;
  }

  /**
  * @dev Subtracts two numbers, throws on overflow (i.e. if subtrahend is greater than minuend).
  */
  function sub(uint256 a, uint256 b) internal pure returns (uint256) {
    assert(b <= a);
    return a - b;
  }

  /**
  * @dev Adds two numbers, throws on overflow.
  */
  function add(uint256 a, uint256 b) internal pure returns (uint256 c) {
    c = a + b;
    assert(c >= a);
    return c;
  }
}


contract Admined {
    
    address public admin;

    event AdminshipTransferred(address indexed _from, address indexed _to);

    function Admined() public {
        admin = msg.sender;
    }

    modifier onlyAdmin {
        require(msg.sender == admin);
        _;
    }

    function transferAdminship(address _newAdmin) public onlyAdmin {
        require(_newAdmin != address(0x0));
         AdminshipTransferred(admin,_newAdmin);
        admin = _newAdmin;
    }
    
}



contract Voting is Admined {
    
    using SafeMath for uint256;
    
    bool public isStopped;
    string public winner;
    
    mapping(address => Voter) public voters;
    Candidate[] public candidates;
    mapping(string => uint) votes;
    
    modifier onlyWhenRunning {
        require(!isStopped);
        _;
    }
    
    modifier onlyWhenStopped {
        require(isStopped);
        _;
    }
    
    //mapping(address => Candidate) private candidates;
     
    struct Voter {
        string name;
        uint voteGivenTo;
        bool hasVoted;
    }
    
    struct Candidate {
        string candidateName;
        string partySymbol;
    }
    
    modifier adminOnly {
        require( msg.sender == admin);
        _;
    }
    
    function Voting() public {
        isStopped = true;
    }
    
    // revert if contract receives ethers
    function() payable public {
        revert();
    }
    
    function addCandidate(string cname, string psym) public adminOnly returns(bool) {
        assert(doesExist(psym));
        Candidate memory c;
        c.candidateName = cname;
        c.partySymbol = psym;
        
        candidates.push(c);
        
        return true;
    }
    
    
    function removeCandidate(string psym) public adminOnly returns(bool) {
        for(uint i=0; i<candidates.length; i++) {
            if(compareStrings(candidates[i].partySymbol,psym)) {
                candidates[i] = candidates[candidates.length - 1];
                candidates.length -= 1;
                return true;
            }
        }
        
        return false;
    }
    
    function addVoter(string nm) public {
        Voter memory v;
        v.name = nm;
        v.hasVoted = false;
        voters[msg.sender] = v;
    }
   
    
    function vote(string psym) onlyWhenRunning public {
        
        assert(isValidCandidate(psym));
        
        Voter memory v;
        v = voters[msg.sender];
        
        // checking if already has voted
        require(!v.hasVoted);
        
        votes[psym] += 1;
        v.hasVoted = true;
        voters[msg.sender] = v;
    }
    
    
    function getVoter() view public returns(string){
        return voters[msg.sender].name;
    }
    
    
    function totalCandidates() view public returns(uint256) {
        return candidates.length;
    }
    
    
    function doesExist(string psym) public view returns(bool){
        for(uint i=0; i<candidates.length; i++) {
            if(compareStrings(candidates[i].partySymbol,psym))
                return false;
        }
        return true;
    }
    
    
    function isValidCandidate(string psym) public view returns(bool){
        for(uint i=0; i<candidates.length; i++) {
            if(compareStrings(candidates[i].partySymbol,psym))
                return true;
        }
        return false;
    }
    
    
    function startElection() onlyAdmin public {
        isStopped = false;
    }
    
    
    function stopElection() onlyAdmin public {
        isStopped = true;
    }
    
    function getResult(string pysm) onlyWhenStopped public view returns(uint256) {
        return votes[pysm];
    }
    

    function compareStrings (string a, string b) pure public returns (bool){
       return keccak256(a) == keccak256(b);
   }
}