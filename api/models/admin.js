// app/models/admin.js

const mongoose = require('mongoose');
const bcrypt   = require('bcryptjs');
const jwt      = require('jsonwebtoken');
const configDB = require('../config/database.js');

var AdminSchema = mongoose.Schema({
    username    : {type:String, unique:true, required: true} ,
    password    : {type:String, required: true},
    email       : {type:String, unique:true, required: true} ,
    fullname       : {type:String, required: true} ,
});


const Admin = module.exports = mongoose.model('Admin', AdminSchema);

module.exports.getAdminById = function(id, callback){
    Admin.findById(id, callback);
}

module.exports.getAdminByUsername = function(username, callback){
    const query = {username: username}
    Admin.findOne(query, callback);
}

module.exports.addAdmin = function(newAdmin, callback){
  bcrypt.genSalt(10, (err, salt) => {
    bcrypt.hash(newAdmin.password, salt, (err, hash) => {
      if (err) throw err;
      newAdmin.password = hash;
      newAdmin.save(callback);
    });
  });
}

module.exports.comparePassword = function(candidatePassword, hash, callback){
  bcrypt.compare(candidatePassword, hash, (err, isMatch) => {
    if(err) throw err;
    callback(null, isMatch);
  });
}