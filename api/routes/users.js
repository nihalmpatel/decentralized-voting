const express = require('express');
const router = express.Router();

const configDB = require('../config/database');
const User = require('../models/user');
const jwt = require('jsonwebtoken');
const bcrypt   = require('bcryptjs');
const speakeasy = require('speakeasy');
const QRCode = require('qrcode');

/* web3
const Web3 = require('web3');
const web3 = new Web3();*/

// Email setup
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_API_KEY || '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');


const liveURL = "localhost:8080";

// Get users

router.get('/', (req,res)=>{
    User.find({}, (err,users) => {
      if (err) {
          res.json({error:'db_error'});
          throw err;
      }
      else {
          res.json(users);
      }
  });
});


router.get('/:id', (req,res) => {
    User.getUserById(req.params.id, (err,user) => {
        if (err) {
            res.json({error:'db_error'});
            throw err;
        }
        else {
            res.json(user);
        }
    });
});


// register user
router.post('/register', (req, res) => {
  console.log(req.body);

    User.findOne({'aadharno': req.body.username}, function (err, user) {
        if (err) throw err;
    
        //checking if user already exists 
        else if (user) {
          res.json({error: 'aadhar_exists'});
        } 
        
        else {
          // check if email id already exists
          User.findOne({'email': req.body.email}, function (err, user) {
            if (err) throw err;
    
            else if (user) {
              res.json({ error: 'email_exists'});
            }
    
            // if everything is fine, register the user
            else {
        
                let newUser = new User({
                    aadharno    : req.body.aadhar_no ,
                    mobileno    : req.body.mobile_no,
                    password    : req.body.password,
                    email       : req.body.email,
                    profile     : {
                        firstname:  req.body.firstname,
                        lastname:   req.body.lastname,
                        gender:     req.body.gender,
                        city:       req.body.city,
                        state:      req.body.state,
                    },
                    isVerified : false,
                    twofactorEnabled: false,
                    kycVerified : false
                })
      
                User.addUser(newUser, (err, user) => {
    
                  if (err) {
                    throw err;
                    res.json({error: 'db_error'});
                  } 
                  
                  else {
                    
                    // jwt token creation
                    var token = jwt.sign({_id: user._id.toHexString()}, configDB.secret);
                    user.verificationToken = token;

                    // ethereum address creation
                    let ac = web3.eth.accounts.create();
                    console.log(ac);

                    console.log('req.body.password : '+ req.body.password);

                    let ac_encrypted = web3.eth.accounts.encrypt(ac.privateKey, req.body.password);
                    user.keyStore = ac_encrypted;

                    user.save((err, data) => {
                      if (err) {
                        res.json({error: 'db_error'});
                      } else {
                        
                        // email object
                        const msg = {
                          to: req.body.email,
                          from: 'donotreply@dvote.com',
                          subject: 'Email Verification',
                          html: '<a href="'+liveURL+'/api/users/verification/'+token+'">'+'Click here to verify your email.'+'</a>',
                        };
                        
                        // Send email verification 
                        sgMail.send(msg);
                        
                        res.json({success: true});
                      }
                    });
                                    
                  }
                });
              
               
            }
          });
        }
      });


});

// Authenticate
router.post('/authenticate', (req, res) => {

    console.log(req.body);
      
    User.findOne({'aadharno': req.body.aadharno}, (err, user) => {
      if (err) throw err;
  
      if (!user) {
        res.json({error: 'invalid_aadharno'});
      }
  
      User.comparePassword(req.body.password, user.password, (err, isMatch) => {
        if (err) throw err;
        if (isMatch) {
  
          // if 2FA enabled
          if(user.twofactorEnabled) {
  
            var verified = speakeasy.totp.verify({
              secret:  user.twofactor.secret || user.twofactor.tempSecret, //secret of the logged in user
              encoding: 'base32',
              token: req.body.token // 2 FA token
            });
  
            if(verified) {
              const token = jwt.sign({ data: user}, configDB.secret, {expiresIn: 86400});
  
              res.json({
                success: true,
                token: 'JWT ' + token,
                user: {
                  id: user._id,
                  email: user.email,    
                  fullname: user.profile.firstname+' '+user.profile.lastname,
                  isVerified : user.isVerified,
                  twofactorEnabled: user.twofactorEnabled
                }
              });
            
            } else {
              res.json({error: 'invalid_token'});
            }
  
          }
  
          else {
  
            const token = jwt.sign({ data: user}, configDB.secret, {expiresIn: 86400});
  
            res.json({
              success: true,
              token: 'JWT ' + token,
              user: {
                id: user._id,
                email: user.email,    
                fullname: user.profile.firstname+' '+user.profile.lastname,
                isVerified : user.isVerified,
                twofactorEnabled: user.twofactorEnabled
              }
            });
          }
        } else {
          res.json({error: 'invalid_password'});
        }
      });
    });
});
  
// Email verification
router.get('/verification/:token', (req, res) => {
  User.findOne({
    'verificationToken': req.params.token
  }, (err, user) => {
    if (user) {
      // if already verified
      if (user.isVerified) {
        res.send('Email already verified! Please logout and login back. Thank you.');
      } else {
        user.isVerified = true;
        user.save((err, data) => {
          if (!err) {
            res.send('Email successfully verified! Please logout and login back. Thank you.');
          } else {
            res.send('Error while verifying email!');
          }
        });
      }
    } else {
      res.send('Invalid Token!');
    }
  })
});

// Resend email
router.get('/resendemail/:id', (req, res) => {
  User.findOne({ _id: req.params.id}, (err, user) => {
    if (user) {
      // if already verified
      if (user.isVerified) {
        res.json({error: "already_verified"});
      } 
      else {
        var token = jwt.sign({_id: user._id.toHexString()}, configDB.secret);
        user.verificationToken = token;
        user.save((err, data) => {
          if (err) {
            res.json({error: 'db_error'});
          } else {
            
            // email object
            const msg = {
              to: user.email,
              from: 'donotreply@dvote.com',
              subject: 'Email Verification',
              html: '<a href="'+liveURL+'/api/users/verification/'+token+'">'+'Click here to verify your email.'+'</a>',
            };
            
            // Send email verification 
            sgMail.send(msg);
            
            res.json({success: true});
          }
        });
      } 
    }
  });
    
});

// forgot password
router.post('/forgotpassword', (req, res) => {
  User.findOne({ email: req.body.email}, (err, user) => {
    if (user) {
      // email object
      const msg = {
        to: user.email,
        from: 'donotreply@dvote.com',
        subject: 'Reset Password',
        html: '<span> Secret code is : '+ user.password +'</span><br/><a href="'+liveURL+'/resetpassword">'+'Click here to reset your passwprd.'+'</a>',
      };
      
      // Send email verification 
      sgMail.send(msg);
      
      res.json({success: true});
    } else {
      res.json({error: "invalid_email"});
    }
    
  });

});

// Reset password
router.post('/resetpassword', (req, res) => {
  User.findOne({ password: req.body.secret}, (err, user) => {
    if (user) {
      
      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(req.body.password, salt, (err, hash) => {
          if (err) throw err;
          user.password = hash;
          user.save((err,savedUser)=>{
            if(err) {
              throw err;
              res.json({error:"db_error"});
            }
            res.json({success: true});
          });
        });
      });
      
    } 
    
    else {
      res.json({error: "invalid_secert"});
    }

    
  });

});


router.post('/verify', (req,res) => {
  User.getUserById(req.body.userid, (err,user) => {
    if (err) {
      res.json({error:'db_error'});
      throw err;
    }

    else if(!user) {
      res.json({error:'invalid_user'});
    }

    else {
      user.kycVerified = true;

      user.save((err,savedUser)=> {
        if (err) {
          res.json({error:'db_error'});
          throw err;
        }
        res.json({success:true});
      });
    }
  });
})

router.post('/disverify', (req,res) => {
  User.getUserById(req.body.userid, (err,user) => {
    if (err) {
      res.json({error:'db_error'});
      throw err;
    }

    else if(!user) {
      res.json({error:'invalid_user'});
    }

    else {
      user.kycVerified = false;

      user.save((err,savedUser)=> {
        if (err) {
          res.json({error:'db_error'});
          throw err;
        }
        res.json({success:true});
      });
    }
  });
})


// ------------------------------------------------------------------
//
// 2 FA
//
// ------------------------------------------------------------------

// setup two factor for logged in user
router.post('/twofactor/setup', (req, res) => {

    User.getUserById(req.body.userid, function(err,user)  {
  
      if(err) {
        res.json({ error: 'db_error'});
        throw err;
      }
  
      else if (!user) {
        res.json({error: 'invalid_user'});
      }
  
      else {
        const secret = speakeasy.generateSecret({length: 10});
        QRCode.toDataURL(secret.otpauth_url, (err, data_url)=>{
  
          let twofactor = {
            secret: "",
            tempSecret: secret.base32,
            dataURL: data_url,
            otpURL: secret.otpauth_url
          };
  
          //save to logged in user.
          user.twofactor = twofactor;
  
          user.save((err, data) => {
            if (err) {
              res.json({error: 'db_error'});
            } else {
              res.json({success: true,tempSecret: secret.base32,dataURL: data_url,otpURL: secret.otpauth_url
            });
            }
          });
          
       
        });
      }
  
    });
  
  });
  
// before enabling totp based 2fa; it's important to verify, so that we don't end up locking the user.
router.post('/twofactor/verify', function(req, res){

  User.getUserById(req.body.userid, function(err,user)  {

    if(err) {
      res.json({ error: 'db_error'});
      throw err;
    }

    else if (!user) {
      res.json({error: 'invalid_user'});
    }

    else {

      var verified = speakeasy.totp.verify({
        secret: user.twofactor.tempSecret || user.twofactor.secret, //secret of the logged in user
        encoding: 'base32',
        token: req.body.token
      });

      if(verified) {

        // first time verification
        if(user.twofactor.tempSecret || !user.twofactorEnabled) {
          user.twofactor.secret = user.twofactor.tempSecret; //set secret, confirm 2fa
          delete user.twofactor.tempSecret;
          user.twofactorEnabled = true;
          user.save((err,savedUser) => {
            if(err) {
              res.json({ error: 'db_error'});
              throw err;
            } else {
              res.json({success:true});
            }            
          });
        }

        else {
          res.json({success:true});
        }
                
      }

      else {
        res.json({error: 'invalid_token'});
      }
      
    }

  });

});

router.get('/home/:id', (req,res) => {
  const Web3 = require('web3');

  const nodeAddress = 'https://ropsten.infura.io/Oppi0FYeLQeMtZGixrTI';
  abiArray = [ { "anonymous": false, "inputs": [ { "indexed": true, "name": "_from", "type": "address" }, { "indexed": true, "name": "_to", "type": "address" } ], "name": "AdminshipTransferred", "type": "event" }, { "constant": false, "inputs": [ { "name": "cname", "type": "string" }, { "name": "psym", "type": "string" } ], "name": "addCandidate", "outputs": [ { "name": "", "type": "bool" } ], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "nm", "type": "string" } ], "name": "addVoter", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "psym", "type": "string" } ], "name": "removeCandidate", "outputs": [ { "name": "", "type": "bool" } ], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [], "name": "startElection", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [], "name": "stopElection", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_newAdmin", "type": "address" } ], "name": "transferAdminship", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "psym", "type": "string" } ], "name": "vote", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "inputs": [], "payable": false, "stateMutability": "nonpayable", "type": "constructor" }, { "payable": true, "stateMutability": "payable", "type": "fallback" }, { "constant": true, "inputs": [], "name": "admin", "outputs": [ { "name": "", "type": "address" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "", "type": "uint256" } ], "name": "candidates", "outputs": [ { "name": "candidateName", "type": "string" }, { "name": "partySymbol", "type": "string" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "a", "type": "string" }, { "name": "b", "type": "string" } ], "name": "compareStrings", "outputs": [ { "name": "", "type": "bool" } ], "payable": false, "stateMutability": "pure", "type": "function" }, { "constant": true, "inputs": [ { "name": "psym", "type": "string" } ], "name": "doesExist", "outputs": [ { "name": "", "type": "bool" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "pysm", "type": "string" } ], "name": "getResult", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "getVoter", "outputs": [ { "name": "", "type": "string" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "isStopped", "outputs": [ { "name": "", "type": "bool" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "psym", "type": "string" } ], "name": "isValidCandidate", "outputs": [ { "name": "", "type": "bool" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "totalCandidates", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "", "type": "address" } ], "name": "voters", "outputs": [ { "name": "name", "type": "string" }, { "name": "voteGivenTo", "type": "uint256" }, { "name": "hasVoted", "type": "bool" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "winner", "outputs": [ { "name": "", "type": "string" } ], "payable": false, "stateMutability": "view", "type": "function" } ];
  
  contractAddress = "0x833EB642fdCC12392BF9f0148bEBee9F2f575F5f";

  const privateKey = '0x7a759e4ab2b401d6d930f1125ebff9f22f14968c11acb15cbf2edaaaacc0b5cc';
  const walletAddress = '0x55820e6B58AB6e70AA6003Ee0b5b43AB76d30f1A';

  const web3 = new Web3(new Web3.providers.HttpProvider(nodeAddress));

  //const contract = web3.eth.contract(abiArray).at(contractAddress);
  const contract = new web3.eth.Contract(abiArray, contractAddress);

  console.log(contract);
  
  
  const query = contract.methods.startElection();
  const encodedABI = query.encodeABI();
  const tx = {
    from: walletAddress,
    to: contractAddress,
    gas: 2000000,
    gasPrice: 100000000000,
    data: encodedABI,
  };

  const account = web3.eth.accounts.privateKeyToAccount(privateKey);

  web3.eth.accounts.signTransaction(tx, privateKey).then(signed => {
    const tran = web3.eth
      .sendSignedTransaction(signed.rawTransaction)
      .on('confirmation', (confirmationNumber, receipt) => {
        console.log('=> confirmation: ' + confirmationNumber);
      })
      .on('transactionHash', hash => {
        console.log('=> hash');
        console.log(hash);
      })
      .on('receipt', receipt => {
        console.log('=> reciept');
        console.log(receipt);
      })
      .on('error', console.error);
    });
 
})





module.exports = router;
