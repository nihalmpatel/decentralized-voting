const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const Admin = require('../models/admin');
const configDB = require('../config/database');

// Get admins

router.get('/', (req, res) => {
    Admin.find({}, (err, admins) => {
        if (err) throw err;
        res.json(admins);
    });
});

router.get('/:id', (req, res) => {
    Admin.getAdminById(req.params.id, (err, admins) => {
        if (err) {
            res.json({ error: 'db_error'});
            throw err;
        } else {
            res.json(admins);
        }
  });
});

// register admin

router.post('/register', (req, res) => {

    Admin.findOne({'username': req.body.username}, function (err, admin) {
        if (err) throw err;

        //checking if user already exists 
        else if (admin) {
            res.json({error: 'user_exists'});
        } 

        else {
            // check if email id already exists
            Admin.findOne({'email': req.body.email}, function (err, admin) {
                if (err) throw err;

                else if (admin) {
                    res.json({ error: 'email_exists'});
                }

                // if everything is fine, register the admin
                else {
                    let newAdmin = new Admin({
                        username: req.body.username,
                        password: req.body.password,
                        email: req.body.email,
                        fullname: req.body.fullname
                    });

                    Admin.addAdmin(newAdmin, (err, admin) => {
                        if (err) {
                            console.log(err)
                            res.json({error: 'db_error'});
                        } 
                        
                        else {
                            admin.save((err, data) => {
                            if (err) {
                                res.json({error: 'db_error'});
                            } else {           
                                res.json({success: true});
                            }
                            });
                        }
                    });
                }
            });
        }
  });

});

// Authenticate
router.post('/authenticate', (req, res) => {

    Admin.getAdminByUsername(req.body.username, (err, admin) => {
    if (err) throw err;

    if (!admin) {
      return res.json({error: 'invalid_username'});
    }

    Admin.comparePassword(req.body.password, admin.password, (err, isMatch) => {
      if (err) throw err;
      if (isMatch) {

        const token = jwt.sign({ data: admin}, configDB.secret, {expiresIn: 86400});

        res.json({
          success: true,
          token: 'JWT ' + token,
          admin: {
            id: admin._id,
            email: admin.email,    
            fullname: admin.fullname,
          }
        });
      } else {
        return res.json({error: 'invalid_password'});
      }
    });
  });
});


module.exports = router;
