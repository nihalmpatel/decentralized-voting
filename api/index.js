const express = require('express');
const app = express();
const mongoose = require('mongoose');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const cors = require('cors');
const configDB = require('./config/database.js');
const port = process.env.PORT || 8080;
const users = require('./routes/users');
const admins = require('./routes/admins');

const Web3 = require('web3');
const web3 = new Web3();


/*
let ac = web3.eth.accounts.create();
console.log(ac);

let ac_encrypted = web3.eth.accounts.encrypt(ac.privateKey, "test");
console.log('Encrypted ac:');
console.log(ac_encrypted);

//let ac_decrypted_false = web3.eth.accounts.decrypt(ac_encrypted, "test1"); // false
let ac_decrypted_true = web3.eth.accounts.decrypt(ac_encrypted, "test"); // true
//console.log('Decrypted ac false:');
//console.log(ac_decrypted_false);
console.log('Decrypted ac true:');
console.log(ac_decrypted_true);
*/

// mongodb configuration
mongoose.connect(configDB.remoteurl, (err, res) => {
  console.log('Connecting Database ...');
  if (err) {
    console.log('Error while connecting to remote db!');
    console.log('Connecting to local db ...');
    mongoose.connect(configDB.localurl, (err, res) => {
      if (err) {
        return console.log('Error while connecting to local db!');
      } else {
        return console.log('Local database successfully connected!');
      }
    });
  } else {
    console.log('Remote database successfully connected!');
  }

});

// set up our express application
app.use(morgan('dev')); // log every request to the console
app.use(bodyParser.json());
app.use(express.static('public'));
app.use(express.static('uploads'));
app.use(cors());

app.use('/api/users', users);
app.use('/api/admins', admins);


// launch 
app.listen(port);
console.log('Server is running on port:' + port);
