// Allows us to use ES6 in our migrations and tests.
require('babel-register')

var HDWalletProvider = require("truffle-hdwallet-provider");
var mnemonic = "city goddess immune amused page cart language robust inject credit math twin";

module.exports = {
  networks: {
    development: {
      host: 'localhost',
      port: 7545,
      network_id: '*' // Match any network id
    },
    ropsten: {
      provider: function() {
        return new HDWalletProvider(mnemonic, "https://ropsten.infura.io/Oppi0FYeLQeMtZGixrTI",0)
      },
      network_id: 3,
      gas: 4600000
    }   
  }
}
