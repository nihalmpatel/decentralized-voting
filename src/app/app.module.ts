import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { Routes, RouterModule } from '@angular/router';

import { SignupComponent } from '../../src/app/components/signup/signup.component';
import { LoginComponent } from '../../src/app/components/login/login.component';

import { DashboardComponent } from './components/dashboard/dashboard.component';
import { IcoMetamaskComponent } from './components/ico-metamask/ico-metamask.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { CreateprofileComponent } from './components/createprofile/createprofile.component';
import { ProfileComponent } from './components/profile/profile.component';
import { VerificationNoticeComponent } from './components/verification-notice/verification-notice.component';
import { AdminLoginComponent } from './components/admin-login/admin-login.component';
import { AdminNavbarComponent } from './components/admin-navbar/admin-navbar.component';
import { AdminUsersComponent } from './components/admin-users/admin-users.component';
import { AdminElectionComponent } from './components/admin-election/admin-election.component';
import { AdminCandidatesComponent } from './components/admin-candidates/admin-candidates.component';
import { ForgotpasswordComponent } from './components/forgotpassword/forgotpassword.component';
import { ResetpasswordComponent } from './components/resetpassword/resetpassword.component';
/*import { Setup2FaComponent } from './components/setup2-fa/setup2-fa.component';
import { Verify2FaComponent } from './components/verify2-fa/verify2-fa.component';*/

// Services
import { ValidationService } from './services/validation.service';
import { AuthService } from './services/auth.service';
import { UserService } from './services/user.service';
import { AuthGuard } from './guards/auth.guards';
import { AdminauthGuard } from './guards/adminauth.guard';
import { AdminService } from './services/admin.service';



const appRoutes: Routes = [
  { path: '', redirectTo:'/login', pathMatch:'full' },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'forgotpassword', component: ForgotpasswordComponent },
  { path: 'resetpassword', component: ResetpasswordComponent },
  { path: 'dashboard', component: DashboardComponent, canActivate:[AuthGuard] },
  { path: 'buytokens', component: IcoMetamaskComponent },
  { path: 'createprofile', component: CreateprofileComponent, canActivate:[AuthGuard] },
  { path: 'editprofile', component: CreateprofileComponent, canActivate:[AuthGuard] },
  { path: 'verificationpending', component: VerificationNoticeComponent },
  { path: 'adminlogin', component: AdminLoginComponent },
  { path: 'users', component: AdminUsersComponent, canActivate:[AdminauthGuard] },
  { path: 'election', component: AdminElectionComponent, canActivate:[AdminauthGuard] },
  { path: 'candidates', component: AdminCandidatesComponent, canActivate:[AdminauthGuard] },
]

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  declarations: [
    AppComponent,
    SignupComponent,
    LoginComponent,
    IcoMetamaskComponent,
    DashboardComponent,
    NavbarComponent,
    CreateprofileComponent,
    ProfileComponent,
    VerificationNoticeComponent,
    AdminLoginComponent,
    AdminNavbarComponent,
    ForgotpasswordComponent,
    ResetpasswordComponent,
    AdminUsersComponent,
    AdminElectionComponent,
    AdminCandidatesComponent,
  ],
  providers: [
    ValidationService,
    AuthService,
    AuthGuard,
    UserService,
    AdminService,
    AdminauthGuard,
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
