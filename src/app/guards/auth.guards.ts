import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthService,private router: Router) { }

  canActivate() {
    if(this.authService.loggedIn()){
      // check for email verifiaction
      if(!this.authService.isVerified()) {
        this.router.navigate(['/verificationpending']);
      } 
      return true;
    }
    else {
      this.router.navigate(['/login']);
      return false;
    }
  }

}
