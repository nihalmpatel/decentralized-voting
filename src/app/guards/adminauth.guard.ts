import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AdminService } from '../services/admin.service';

@Injectable()
export class AdminauthGuard implements CanActivate {

  constructor(private adminService: AdminService, private router: Router) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      if(this.adminService.loggedIn()){
        return true;
      }
      else {
        this.router.navigate(['/adminlogin']);
        return false;
      }
  }
}
