import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { AuthService } from './auth.service';
import { tokenNotExpired } from 'angular2-jwt';
import 'rxjs/add/operator/map';

@Injectable()
export class AdminService {

  auth_token;
  auth_admin;

  constructor( 
    private http: Http,
    private authService: AuthService
  ) { }

  getAdmin(id) {
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.get(this.authService.endpoint('admins/'+id),{headers:headers})
    .map(res=>res.json())
  }

  getAdmins() {
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.get(this.authService.endpoint('admins/'),{headers:headers})
    .map(res=>res.json());
  }

  getUsers() {
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.get(this.authService.endpoint('users/'),{headers:headers})
    .map(res=>res.json());
  }

  addAdmin(admin) {
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post(this.authService.endpoint('admins/register'),admin,{headers:headers})
    .map(res=>res.json());
  }

  login(admin) {
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post(this.authService.endpoint('admins/authenticate'),admin,{headers:headers})
    .map(res=>res.json());
  }

  storeData(token,admin) {
    localStorage.setItem('auth_token',token);
    localStorage.setItem('auth_admin',JSON.stringify(admin));
    this.auth_token=token;
    this.auth_admin=admin;
  }

  loadToken() {
    this.auth_token=localStorage.getItem('auth_token');
  }

  loggedIn() {
    return tokenNotExpired('auth_token');
  }

  logout() {
    this.auth_token=null;
    this.auth_admin=null;
    localStorage.clear();
  }

}
