import { Injectable, OnInit } from '@angular/core';
const Web3 = require('web3');

declare var window: any;

@Injectable()
export class ContractService implements OnInit {

  // TODO add proper types these variables
  account: any;
  accounts: any;
  web3: any;
  contract;
  abiArray = [ { "anonymous": false, "inputs": [ { "indexed": true, "name": "_from", "type": "address" }, { "indexed": true, "name": "_to", "type": "address" } ], "name": "AdminshipTransferred", "type": "event" }, { "constant": false, "inputs": [ { "name": "cname", "type": "string" }, { "name": "psym", "type": "string" } ], "name": "addCandidate", "outputs": [ { "name": "", "type": "bool" } ], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "nm", "type": "string" } ], "name": "addVoter", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "psym", "type": "string" } ], "name": "removeCandidate", "outputs": [ { "name": "", "type": "bool" } ], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [], "name": "startElection", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [], "name": "stopElection", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_newAdmin", "type": "address" } ], "name": "transferAdminship", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "psym", "type": "string" } ], "name": "vote", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "inputs": [], "payable": false, "stateMutability": "nonpayable", "type": "constructor" }, { "payable": true, "stateMutability": "payable", "type": "fallback" }, { "constant": true, "inputs": [], "name": "admin", "outputs": [ { "name": "", "type": "address" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "", "type": "uint256" } ], "name": "candidates", "outputs": [ { "name": "candidateName", "type": "string" }, { "name": "partySymbol", "type": "string" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "a", "type": "string" }, { "name": "b", "type": "string" } ], "name": "compareStrings", "outputs": [ { "name": "", "type": "bool" } ], "payable": false, "stateMutability": "pure", "type": "function" }, { "constant": true, "inputs": [ { "name": "psym", "type": "string" } ], "name": "doesExist", "outputs": [ { "name": "", "type": "bool" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "pysm", "type": "string" } ], "name": "getResult", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "getVoter", "outputs": [ { "name": "", "type": "string" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "isStopped", "outputs": [ { "name": "", "type": "bool" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "psym", "type": "string" } ], "name": "isValidCandidate", "outputs": [ { "name": "", "type": "bool" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "totalCandidates", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "", "type": "address" } ], "name": "voters", "outputs": [ { "name": "name", "type": "string" }, { "name": "voteGivenTo", "type": "uint256" }, { "name": "hasVoted", "type": "bool" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "winner", "outputs": [ { "name": "", "type": "string" } ], "payable": false, "stateMutability": "view", "type": "function" } ];
  admin = "0x627306090abaB3A6e1400e9345bC60c78a8BEf57";
  contractAddress = "0x345ca3e014aaf5dca488057592ee47305d9b3e10";

  ngOnInit() {
    this.checkAndInstantiateWeb3();
  }
  
  constructor() { }


  checkAndInstantiateWeb3 = () => {
    // Checking if Web3 has been injected by the browser (Mist/MetaMask)
    if (typeof window.web3 !== 'undefined') {
      console.log(
        'Using web3 detected from external source.'
      );


      // Use Local web3 provider
      this.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:7545"));

    } else {
      alert('No web3 detected. Kindly check your MetaMask settings.');
    }
  
  };


  initializeContractInstance = () => {

    this.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:7545"));

    //this.contract = this.web3.eth.contract(this.abiArray).at(this.contractAddress);

    // Get the initial account balance so it can be displayed. 

    this.web3.eth.getAccounts((err, accs) => {
      if (err != null) {
        alert('There was an error fetching your accounts.');
        return;
      }

      if (accs.length === 0) {
        alert(
          'Couldn\'t get any accounts! Make sure your Ethereum client is configured correctly.'
        );
        return;
      }

      this.accounts = accs;
      this.account = this.accounts[0];

      /*
      console.log('Accounts:');
      console.log(this.accounts);

      console.log('Smart contract instance:');
      console.log(this.contract); */

      return this.web3.eth.contract(this.abiArray).at(this.contractAddress);
    });
  };

}
