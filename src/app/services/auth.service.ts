import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { tokenNotExpired } from 'angular2-jwt';

@Injectable()
export class AuthService {
  url = "http://localhost:8080/api/";

  auth_token;
  auth_user;

  constructor( private http: Http ) { }

  endpoint(ep) {
    return this.url+ep;
  }

  signupUser(user) {
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post(this.endpoint('users/register'),user,{headers:headers})
    .map(res=>res.json());
  }

  login(user) {
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post(this.endpoint('users/authenticate'),user,{headers:headers})
    .map(res=>res.json());
  }

  storeUserData(token,user) {
    localStorage.setItem('auth_token',token);
    localStorage.setItem('auth_user',JSON.stringify(user));
    this.auth_token=token;
    this.auth_user=user;
  }

  loadToken() {
    this.auth_token=localStorage.getItem('auth_token');
  }

  loggedIn() {
    return tokenNotExpired('auth_token');
  }

  isVerified() {
    return JSON.parse(localStorage.getItem("auth_user")).isVerified;
  }

  logoutUser() {
    this.auth_token=null;
    this.auth_user=null;
    localStorage.clear();
  }

}
