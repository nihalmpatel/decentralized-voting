import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { AuthService } from './auth.service';
import 'rxjs/add/operator/map';

@Injectable()
export class UserService {

  constructor( 
    private http: Http,
    private authService: AuthService
  ) { }

  getUser(id) {
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.get(this.authService.endpoint('users/'+id),{headers:headers})
    .map(res=>res.json())
  }

  createProfile(profile) {
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post(this.authService.endpoint('users/createprofile'),profile,{headers:headers})
    .map(res=>res.json());
  }

  resendEmail(id) {
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.get(this.authService.endpoint('users/resendemail/'+id),{headers:headers})
    .map(res=>res.json())
  }

  forgotPassword(email) {
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post(this.authService.endpoint('users/forgotpassword'),{ "email" : email },{headers:headers})
    .map(res=>res.json());
  }

  resetPassword(newpassword) {
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post(this.authService.endpoint('users/resetpassword'),newpassword,{headers:headers})
    .map(res=>res.json());
  }

  getUsers() {
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.get(this.authService.endpoint('users/'),{headers:headers})
    .map(res=>res.json());
  }

  verifyUser(userid) {
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post(this.authService.endpoint('users/verify'),{userid : userid},{headers:headers})
    .map(res=>res.json());
  }

  DisverifyUser(userid) {
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post(this.authService.endpoint('users/disverify'),{userid : userid},{headers:headers})
    .map(res=>res.json());
  }

  setup2FA(userid) {
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post(this.authService.endpoint('users/twofactor/setup'),{userid : userid},{headers:headers})
    .map(res=>res.json());
  }


}
