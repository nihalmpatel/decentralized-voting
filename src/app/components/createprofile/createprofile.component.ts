import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { ValidationService } from '../../services/validation.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-createprofile',
  templateUrl: './createprofile.component.html',
  styleUrls: ['./createprofile.component.css']
})
export class CreateprofileComponent implements OnInit {

  fullname;
  mobile;
  country = "IN";
  etherbase;
  userid;

  constructor( 
    private userservice : UserService, 
    private router : Router,
    private validationservice : ValidationService 
  ) { 
    
  }

  ngOnInit() {
    this.userid = JSON.parse(localStorage.getItem("auth_user")).id;
  }

  onSave() {

    let profile = {
      userid : this.userid,
      fullname : this.fullname,
      mobile : this.mobile,
      country : this.country,
      etherbase : this.etherbase
    }

    if (!this.validationservice.validateProfile(profile)) {
      alert("All fields are mendatory!");
      return;
    }

    this.userservice.createProfile(profile).subscribe(data => {
      if(data.success) {
        alert('Profile successfully updated!');
        this.router.navigate(['/booktokens']);
      } else {
        alert('Error while updating data! Please try again.');
      }
    });
  }

}
