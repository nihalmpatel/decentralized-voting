import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.css']
})
export class ForgotpasswordComponent implements OnInit {

  email;

  constructor( private userService : UserService, private router : Router ) { }

  ngOnInit() {
  }

  forgotpasswordClick() {
    this.userService.forgotPassword(this.email).subscribe( data => {
      if(data.error == "invalid_email") {
        alert('Invalid email address!');
      }
      else if(data.success) {
        alert('Email sent to: '+this.email);
        this.router.navigate(['/resetpassword']);
      } 
      else {
        alert('Something went wrong!');
      } 
    })
  }

}
