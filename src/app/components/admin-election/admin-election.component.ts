import { Component, HostListener, NgZone, OnInit } from '@angular/core';

const Web3 = require('web3');

declare var window: any;

@Component({
  selector: 'app-admin-election',
  templateUrl: './admin-election.component.html',
  styleUrls: ['./admin-election.component.css']
})
export class AdminElectionComponent implements OnInit {

  web3: any;

  candidates = [];
  result;
  isResultDeclared = false;
  maxVote = 0;
  tie = false;
  winner;

  cname;
  pname;
  psym;

  isStopped;

  super = this;

  // TODO add proper types these variables
  account: any;
  accounts: any;
  
  contract;
  abiArray = [ { "anonymous": false, "inputs": [ { "indexed": true, "name": "_from", "type": "address" }, { "indexed": true, "name": "_to", "type": "address" } ], "name": "AdminshipTransferred", "type": "event" }, { "constant": false, "inputs": [ { "name": "cname", "type": "string" }, { "name": "psym", "type": "string" } ], "name": "addCandidate", "outputs": [ { "name": "", "type": "bool" } ], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "nm", "type": "string" } ], "name": "addVoter", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "psym", "type": "string" } ], "name": "removeCandidate", "outputs": [ { "name": "", "type": "bool" } ], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [], "name": "startElection", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [], "name": "stopElection", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_newAdmin", "type": "address" } ], "name": "transferAdminship", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "psym", "type": "string" } ], "name": "vote", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "inputs": [], "payable": false, "stateMutability": "nonpayable", "type": "constructor" }, { "payable": true, "stateMutability": "payable", "type": "fallback" }, { "constant": true, "inputs": [], "name": "admin", "outputs": [ { "name": "", "type": "address" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "", "type": "uint256" } ], "name": "candidates", "outputs": [ { "name": "candidateName", "type": "string" }, { "name": "partySymbol", "type": "string" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "a", "type": "string" }, { "name": "b", "type": "string" } ], "name": "compareStrings", "outputs": [ { "name": "", "type": "bool" } ], "payable": false, "stateMutability": "pure", "type": "function" }, { "constant": true, "inputs": [ { "name": "psym", "type": "string" } ], "name": "doesExist", "outputs": [ { "name": "", "type": "bool" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "pysm", "type": "string" } ], "name": "getResult", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "getVoter", "outputs": [ { "name": "", "type": "string" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "hasVoted", "outputs": [ { "name": "", "type": "bool" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "isStopped", "outputs": [ { "name": "", "type": "bool" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "psym", "type": "string" } ], "name": "isValidCandidate", "outputs": [ { "name": "", "type": "bool" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "totalCandidates", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "", "type": "address" } ], "name": "voters", "outputs": [ { "name": "name", "type": "string" }, { "name": "voteGivenTo", "type": "uint256" }, { "name": "hasVoted", "type": "bool" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "winner", "outputs": [ { "name": "", "type": "string" } ], "payable": false, "stateMutability": "view", "type": "function" } ];
  admin = "0x627306090abaB3A6e1400e9345bC60c78a8BEf57";
  contractAddress = "0xecfcab0a285d3380e488a39b4bb21e777f8a4eac"; 

  constructor() { }

  ngOnInit() {
    this.checkAndInstantiateWeb3();
    this.onReady();
  }


  checkAndInstantiateWeb3 = () => {
    // Checking if Web3 has been injected by the browser (Mist/MetaMask)
    if (typeof window.web3 !== 'undefined') {
      console.log(
        'Using web3 detected from external source.'
      );


      // Use Local web3 provider
      this.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:7545"));

      // Use Mist/MetaMask's provider
      //this.web3 = new Web3(window.web3.currentProvider);

      /*
      this.web3.version.getNetwork((err, netId) => {
        switch (netId) {
          case "1":
            console.log('This is mainnet')
            break
          case "3":
            alert('Alert: This is a Test Network. ATE/ETH amount displayed here doesn\'t hold any real value! Kindly select Main Etherum Network in MetaMask to buy Tokens.' );
            break 
          default:
            alert('Error: Please choose Main Ethereum Network or Ropsten Test Network in your Metamask.');
        }
      });
      */

    } else {
      alert('No web3 detected. Kindly check your MetaMask settings.');
    }
  
  };



  onReady = () => {

    // Get the initial account balance so it can be displayed. 

    this.web3.eth.getAccounts((err, accs) => {
      if (err != null) {
        alert('There was an error fetching your accounts.');
        return;
      }

      if (accs.length === 0) {
        alert(
          'Couldn\'t get any accounts! Make sure your Ethereum client is configured correctly.'
        );
        return;
      }

      this.accounts = accs;
      this.account = this.accounts[0];

      console.log('Accounts:');
      console.log(this.accounts);

      
      this.contract = this.web3.eth.contract(this.abiArray).at(this.contractAddress);

      console.log('Smart contract instance:');
      console.log(this.contract);
      this.checkIfStopped();
      this.getCandidates();
    });
  };


  async getCandidates() {

    let candidates = [];

    this.totalCandidates().then((val) => {

      for(let i=0; i< val ;i++) {
        this.contract.candidates.call(i,{from:this.account},function(err,res) {
          // only party symbol
          candidates.push(res);
        });
      }
      
    });

    this.candidates = candidates;
    console.log(this.candidates);
    
  }


  showResult() {

    
    let result =[];
    let temp = [];
    let winner;
    let max = this.maxVote;

    this.totalCandidates().then((val) => {

      for(let i=0; i< val ;i++) {
        let candidates = this.candidates;
        let psym = this.candidates[i][1];   
        this.contract.getResult(psym,{from:this.account},function(err,res) { 
          temp.push(candidates[i][0]);
          temp.push(candidates[i][1]);
          temp.push(res.toNumber());

          result.push(temp);
          temp = [];


          if(res.toNumber() > max) {
            winner = psym;
            max = res.toNumber();
            console.log(winner);
            console.log(max);
          }
        });

        this.maxVote = max;
        this.winner = winner;
      }
      
      this.isResultDeclared = true;
    });

    this.result = result;
    
    

    console.log(this.result);
    console.log(this.winner);
    console.log(this.maxVote);
  
  }


  async totalCandidates() {

    let promise = new Promise((resolve, reject) => {

      this.contract.totalCandidates.call({from:this.account},function(err,res) {
        resolve(res.toNumber());
      });  

    });

    return await promise;

  }


  async checkIfStopped() {

    let promise = new Promise((resolve, reject) => {

      this.contract.isStopped.call({from:this.account},function(err,res) {
        resolve(res);
      });  

    });

    let result = await promise
    console.log('result :'+result);
    
    this.isStopped = result;

  }


  startElection() {

    this.contract.startElection({from:this.admin},(err,tx) => {
      if(tx) {
        alert('Transaction sent! '+tx);
        window.location.reload();
      }
      
    })

  }

  
  stopElection() {

    this.contract.stopElection({from:this.admin},(err,tx) => {
      if(tx) {
        alert('Transaction sent! '+tx);
        window.location.reload();
      }
      
    })

  }



}
