import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  stage = 0;
  email;
  password;
  cpassword;

  firstname : String = "";
  lastname : String = "";
  aadhar_no : Number ;
  mobile_no : Number;
  zipcode : Number;
  street : String = "";
  city : String = "";
  state : String = "";
  otp : Number;

  constructor(private router: Router, private authService : AuthService) { }

  ngOnInit() {
  }

  stageOneSubmit() {
    if(this.password !== this.cpassword) {
      alert('Passwords does not match!');
    } else {
      this.stage = 1;
    } 
  }

  onOtpClick() {
    if(this.firstname.length <= 0 || this.lastname.length <= 0 || this.aadhar_no.toString().length <= 0 || this.mobile_no.toString().length <= 0 || this.zipcode.toString().length <= 0|| this.street.length <= 0 || this.city.length <= 0 || this.state.length <= 0) {
      alert('Please fill all the fields!');
    }
  }

  signupSubmit() {
    let user = {
      email: this.email,
      firstname : this.firstname,
      lastname : this.lastname,
      password : this.password,
      aadhar_no : this.aadhar_no,
      mobile_no : this.mobile_no,
      zipcode : this.zipcode,
      state : this.state,
      city : this.city,
      street : this.street
    }

    console.log(user)

    this.authService.signupUser(user).subscribe(data => {
      if(data.success) {
        alert('Sign up successful! Kindly check your email.');
        this.router.navigate(['/dashboard']);
      }
      else {
        if(data.error == "aadhar_exists") {
          alert('Aaddhar card is already linked with an account. Kindly login');
          return false;
        }

        if(data.error == "email_exists") {
          alert('Email id is registered!');
          return false;
        }

      }
    })

  }

}
