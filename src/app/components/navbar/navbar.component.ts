import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  user;
  constructor(private authService: AuthService,private router: Router) { }

  ngOnInit() {
    this.user=JSON.parse(localStorage.getItem('auth_user'));
  }

  onLogoutClick(){
    this.authService.logoutUser();
    this.router.navigate(['/login']);
  }

}
