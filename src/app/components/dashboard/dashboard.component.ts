import { Component, OnInit  } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

const Web3 = require('web3');

declare var window: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  user;

  web3: any;

  candidates = [];
  
  isStopped;
  hasVoted;

  // TODO add proper types these variables
  account: any;
  accounts: any;
  
  contract;
  abiArray = [ { "anonymous": false, "inputs": [ { "indexed": true, "name": "_from", "type": "address" }, { "indexed": true, "name": "_to", "type": "address" } ], "name": "AdminshipTransferred", "type": "event" }, { "constant": false, "inputs": [ { "name": "cname", "type": "string" }, { "name": "psym", "type": "string" } ], "name": "addCandidate", "outputs": [ { "name": "", "type": "bool" } ], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "nm", "type": "string" } ], "name": "addVoter", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "psym", "type": "string" } ], "name": "removeCandidate", "outputs": [ { "name": "", "type": "bool" } ], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [], "name": "startElection", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [], "name": "stopElection", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_newAdmin", "type": "address" } ], "name": "transferAdminship", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "psym", "type": "string" } ], "name": "vote", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "inputs": [], "payable": false, "stateMutability": "nonpayable", "type": "constructor" }, { "payable": true, "stateMutability": "payable", "type": "fallback" }, { "constant": true, "inputs": [], "name": "admin", "outputs": [ { "name": "", "type": "address" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "", "type": "uint256" } ], "name": "candidates", "outputs": [ { "name": "candidateName", "type": "string" }, { "name": "partySymbol", "type": "string" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "a", "type": "string" }, { "name": "b", "type": "string" } ], "name": "compareStrings", "outputs": [ { "name": "", "type": "bool" } ], "payable": false, "stateMutability": "pure", "type": "function" }, { "constant": true, "inputs": [ { "name": "psym", "type": "string" } ], "name": "doesExist", "outputs": [ { "name": "", "type": "bool" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "pysm", "type": "string" } ], "name": "getResult", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "getVoter", "outputs": [ { "name": "", "type": "string" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "hasVoted", "outputs": [ { "name": "", "type": "bool" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "isStopped", "outputs": [ { "name": "", "type": "bool" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "psym", "type": "string" } ], "name": "isValidCandidate", "outputs": [ { "name": "", "type": "bool" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "totalCandidates", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "", "type": "address" } ], "name": "voters", "outputs": [ { "name": "name", "type": "string" }, { "name": "voteGivenTo", "type": "uint256" }, { "name": "hasVoted", "type": "bool" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "winner", "outputs": [ { "name": "", "type": "string" } ], "payable": false, "stateMutability": "view", "type": "function" } ];
  contractAddress = "0xecfcab0a285d3380e488a39b4bb21e777f8a4eac"; 

  constructor(private authService: AuthService,private router: Router) { }

  ngOnInit() {
    this.user=JSON.parse(localStorage.getItem('auth_user')); 
    this.checkAndInstantiateWeb3();
    this.onReady();  
  }

  onLogoutClick(){
    this.authService.logoutUser();
    this.router.navigate(['/login']);
  }

  checkAndInstantiateWeb3 = () => {
    // Checking if Web3 has been injected by the browser (Mist/MetaMask)
    if (typeof window.web3 !== 'undefined') {
      console.log(
        'Using web3 detected from external source.'
      );


      // Use Local web3 provider
      this.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:7545"));

      // Use Mist/MetaMask's provider
      //this.web3 = new Web3(window.web3.currentProvider);

      /*
      this.web3.version.getNetwork((err, netId) => {
        switch (netId) {
          case "1":
            console.log('This is mainnet')
            break
          case "3":
            alert('Alert: This is a Test Network. ATE/ETH amount displayed here doesn\'t hold any real value! Kindly select Main Etherum Network in MetaMask to buy Tokens.' );
            break 
          default:
            alert('Error: Please choose Main Ethereum Network or Ropsten Test Network in your Metamask.');
        }
      });
      */

    } else {
      alert('No web3 detected. Kindly check your MetaMask settings.');
    }
  
  };



  onReady = () => {

    // Get the initial account balance so it can be displayed. 

    this.web3.eth.getAccounts((err, accs) => {
      if (err != null) {
        alert('There was an error fetching your accounts.');
        return;
      }

      if (accs.length === 0) {
        alert(
          'Couldn\'t get any accounts! Make sure your Ethereum client is configured correctly.'
        );
        return;
      }

      this.accounts = accs;
      var index = prompt('Please choose ethereum account:');
      this.account = this.accounts[index];

      console.log('Accounts:');
      console.log(this.accounts);

      
      this.contract = this.web3.eth.contract(this.abiArray).at(this.contractAddress);

      console.log('Smart contract instance:');
      console.log(this.contract);

      this.getCandidates();
      this.checkIfStopped();
      this.checkIfVoted();
    });
  };


  async getCandidates() {

    let candidates = [];

    this.totalCandidates().then((val) => {

      for(let i=0; i< val ;i++) {
        this.contract.candidates.call(i,{from:this.account},function(err,res) {
          // only party symbol
          candidates.push(res);
        });
      }
      
    });

    this.candidates = candidates;
    console.log(this.candidates);
    
  }


  async totalCandidates() {

    let promise = new Promise((resolve, reject) => {

      this.contract.totalCandidates.call({from:this.account},function(err,res) {
        resolve(res.toNumber());
      });  

    });

    return await promise;

  }


  onvoteCandidate(psym) {
    this.contract.vote.sendTransaction(psym, {from: this.account}, function(err,tx) {
      alert('Transaction sent! '+tx);
    });
  }


  async checkIfStopped() {

    let promise = new Promise((resolve, reject) => {

      this.contract.isStopped.call({from:this.account},function(err,res) {
        resolve(res);
      });  

    });

    let result = await promise
    console.log('result :'+result);
    
    this.isStopped = result;

  }

  async checkIfVoted() {

    let promise = new Promise((resolve, reject) => {

      this.contract.hasVoted({from:this.account},function(err,res) {
        resolve(res);
      });  

    });

    let result = await promise
    
    this.hasVoted = result;

  }


}
