import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService } from '../../services/admin.service';

@Component({
  selector: 'app-admin-navbar',
  templateUrl: './admin-navbar.component.html',
  styleUrls: ['../navbar/navbar.component.css']
})
export class AdminNavbarComponent implements OnInit {
  admin;
  constructor(private adminService: AdminService, private router: Router) { }

  ngOnInit() {
    this.admin=JSON.parse(localStorage.getItem('auth_admin'));
  }

  onLogoutClick(){
    this.adminService.logout();
    this.router.navigate(['/adminlogin']);
  }

}
