import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-verification-notice',
  templateUrl: './verification-notice.component.html',
  styleUrls: ['./verification-notice.component.css']
})
export class VerificationNoticeComponent implements OnInit {

  userid;
  email;

  constructor( 
    private userService : UserService, 
    private authService : AuthService,
    private router : Router 
  ) { }

  ngOnInit() {
    this.userid = JSON.parse(localStorage.getItem("auth_user")).id;
    this.email = JSON.parse(localStorage.getItem("auth_user")).email;
  }

  onVerification() {
    this.userService.resendEmail(this.userid).subscribe(data=>{
      console.log(data);
      if(data.error) {
        if(data.error == "already_verified") {
          alert('Email already verified! Please logout and login back.');
          return;
        }
        alert('Error while resending email verification');
      }
      else {
        alert('Email successfully sent to: '+this.email);
      }
    })
  }

  onLogoutClick(){
    this.authService.logoutUser();
    this.router.navigate(['/login']);
  }

}
