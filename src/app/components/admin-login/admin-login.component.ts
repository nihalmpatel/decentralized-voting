import { Component, OnInit } from '@angular/core';
import { ValidationService } from '../../services/validation.service';
import { AdminService } from '../../services/admin.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {

  username: String;
  password: String;

  constructor( 
    private validationService: ValidationService, 
    private adminService: AdminService,
    private router: Router 
  ) { }

  ngOnInit() {
    if(this.adminService.loggedIn()){
      this.router.navigate(['/users']);
    }
  }

  loginSubmit() {

    const admin = {
      username:this.username,
      password:this.password
    }

    if(!this.validationService.validateLogin(admin)) {
      alert('All fields are compulsory!');
      return false;
    }

    this.adminService.login(admin).subscribe(data=> {    
      if(data.success){
        this.adminService.storeData(data.token,data.admin);
        this.router.navigate(['/users']);
      }
      
      else{
        if(data.error=="invalid_username") {
          alert('Enter valid username!');
          return false;
        }
        if(data.error=="invalid_password") {
          alert('Enter valid password!');
          return false;
        }
      }
    });
  }

}
