import { Component, HostListener, NgZone } from '@angular/core';
const Web3 = require('web3');

declare var window: any;

@Component({
  selector: 'app-ico-metamask',
  templateUrl: './ico-metamask.component.html',
  styleUrls: ['./ico-metamask.component.css']
})

export class IcoMetamaskComponent {

   // TODO add proper types these variables
   account: any;
   accounts: any;
   web3: any;
   contract;
   contractAddress = "0xb14f1d36f04cdd15ec4c6506a02a2d329d902518";
   contractOwner = "0xdee7d782fa2645070e3c15cabf8324a0ccceac78";
   rate: number = 600;
   balance: number;
   availableSupply: number;
   
   sendingAmount: number;
   etherAmount: number;
   recipientAddress: string;
   status: string;
 
   constructor(private _ngZone: NgZone) {
     
   }
 
   @HostListener('window:load')
   windowLoaded() {
     this.checkAndInstantiateWeb3();
     this.onReady();
   }
 
   checkAndInstantiateWeb3 = () => {
     // Checking if Web3 has been injected by the browser (Mist/MetaMask)
     if (typeof window.web3 !== 'undefined') {
       console.log(
         'Using web3 detected from external source.'
       );
       // Use Mist/MetaMask's provider
       this.web3 = new Web3(window.web3.currentProvider);
       this.web3.version.getNetwork((err, netId) => {
         switch (netId) {
           case "1":
             console.log('This is mainnet')
             break
           case "3":
             alert('Alert: This is a Test Network. ATE/ETH amount displayed here doesn\'t hold any real value! Kindly select Main Etherum Network in MetaMask to buy Tokens.' );
             break 
           default:
             alert('Error: Please choose Main Ethereum Network or Ropsten Test Network in your Metamask.');
         }
       });
 
     } else {
       alert('No web3 detected. Kindly check your MetaMask settings.');
     }
   
   };
 
   onReady = () => {
     // Bootstrap the MetaCoin abstraction for Use.
     //this.MetaCoin.setProvider(this.web3.currentProvider);
 
     // Get the initial account balance so it can be displayed. 
 
     this.web3.eth.getAccounts((err, accs) => {
       if (err != null) {
         alert('There was an error fetching your accounts.');
         return;
       }
 
       if (accs.length === 0) {
         alert(
           'Couldn\'t get any accounts! Make sure your Ethereum client is configured correctly.'
         );
         return;
       }
 
       this.accounts = accs;
       this.account = this.accounts[0];
 
       let abiArray = [ { "anonymous": false, "inputs": [ { "indexed": true, "name": "tokenOwner", "type": "address" }, { "indexed": true, "name": "spender", "type": "address" }, { "indexed": false, "name": "tokens", "type": "uint256" } ], "name": "Approval", "type": "event" }, { "constant": false, "inputs": [], "name": "buyTokens", "outputs": [], "payable": true, "stateMutability": "payable", "type": "function" }, { "anonymous": false, "inputs": [ { "indexed": true, "name": "from", "type": "address" }, { "indexed": true, "name": "to", "type": "address" }, { "indexed": false, "name": "tokens", "type": "uint256" } ], "name": "Transfer", "type": "event" }, { "constant": false, "inputs": [ { "name": "spender", "type": "address" }, { "name": "tokens", "type": "uint256" } ], "name": "approve", "outputs": [ { "name": "success", "type": "bool" } ], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "to", "type": "address" }, { "name": "tokens", "type": "uint256" } ], "name": "transfer", "outputs": [ { "name": "success", "type": "bool" } ], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "from", "type": "address" }, { "name": "to", "type": "address" }, { "name": "tokens", "type": "uint256" } ], "name": "transferFrom", "outputs": [ { "name": "success", "type": "bool" } ], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "inputs": [], "payable": false, "stateMutability": "nonpayable", "type": "constructor" }, { "payable": true, "stateMutability": "payable", "type": "fallback" }, { "constant": true, "inputs": [], "name": "_totalSupply", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "tokenOwner", "type": "address" }, { "name": "spender", "type": "address" } ], "name": "allowance", "outputs": [ { "name": "remaining", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "tokenOwner", "type": "address" } ], "name": "balanceOf", "outputs": [ { "name": "balance", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "decimals", "outputs": [ { "name": "", "type": "uint8" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "name", "outputs": [ { "name": "", "type": "string" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "owner", "outputs": [ { "name": "", "type": "address" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "RATE", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "symbol", "outputs": [ { "name": "", "type": "string" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "totalSupply", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" } ];
       this.contract = this.web3.eth.contract(abiArray).at(this.contractAddress);
 
       this.refreshBalance(this.web3,this.account);
 
     });
   };
 
   refreshBalance = (web3_temp,ac) => {
 
     this.contract.balanceOf(this.contractOwner,function(err,bal){ 
       if(err) {
         alert('Couldn\'t get Total Supply. Something Went Wrong!');
       } 
       else {
         document.getElementById("available_token").innerHTML = web3_temp.fromWei(bal,"ether").toString();
       }
     });
       
     this.contract.balanceOf(this.account,function(err,bal){  
       if(err) {
         alert('Couldn\'t get your balance. Something Went Wrong!');
       } else {
         document.getElementById("balance").innerHTML = web3_temp.fromWei(bal,"ether").toString();
       }    
       
     });
 
   };
 
   setStatus = message => {
     this.status = message;
   };
 
   buyTokens = () => {
 
     this.contract.buyTokens({from:this.account,value:this.web3.toWei(this.etherAmount)},function(err,transaction){
       
       if(err) {
         alert('Error: '+err);
         document.getElementById("transaction").innerHTML = 'Transaction Failed!';
       } else {
         document.getElementById("transaction").innerHTML = 'Transaction Successful! : <a href="http://ropsten.etherscan.io/tx/'+transaction+'">'+ transaction +'</a>';
       }
       
     });
 
     /*
     this.contract.transfer(address,amount,{from:this.account},(err,tra)=>{
       console.log(tra);
     }); */
 
   };
 
   calculatePrice (event) {
       this.etherAmount = event.target.value / this.rate;
   };
 
   isValid () {
     if( this.etherAmount && this.canBeNumber(this.etherAmount.toString()) ) {
       return true;
     } else { return false; }
   }
 
  canBeNumber(str: string) {
    if (!str) {
      return false;
    }
    return !isNaN(+str);
  }

}
