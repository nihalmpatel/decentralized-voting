import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IcoMetamaskComponent } from './ico-metamask.component';

describe('IcoMetamaskComponent', () => {
  let component: IcoMetamaskComponent;
  let fixture: ComponentFixture<IcoMetamaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IcoMetamaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IcoMetamaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
