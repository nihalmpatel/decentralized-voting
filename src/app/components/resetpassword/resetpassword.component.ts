import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.css']
})
export class ResetpasswordComponent implements OnInit {

  secret;
  password;
  cpassword;

  constructor( private userService : UserService, private router : Router) { }

  ngOnInit() {
  }

  resetPassword() {

    let newpassword = {
      secret : this.secret,
      password : this.password
    }

    if(this.validation()) {
      this.userService.resetPassword(newpassword).subscribe(data => {
        if(data.error == "invalid_secert") {
          alert('Invalid secret!');
        }
        else {
          alert('Password changed successfully!');
          this.router.navigate(['/login']);
        }
      })
    }

  }

  validation() {
    if(this.secret == undefined || this.password == undefined || this.cpassword == undefined) {
      alert('All fields are mandatory!');
      return false;
    }

    if(this.password != this.cpassword) {
      alert('Password and confirm password should be same!');
      return false;
    }

    return true;
  }

}
