import { Component, OnInit } from '@angular/core';
import { ValidationService } from '../../services/validation.service';
import { AuthService } from '../../services/auth.service';
import { UserService } from '../../services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  aadharno: number;
  password;
  token;

  constructor( 
    private validationService: ValidationService, 
    private authService: AuthService,
    private userService: UserService,
    private router: Router 
  ) { }

  ngOnInit() {
    if(this.authService.loggedIn()){
      this.router.navigate(['/dashboard']);
    }
  }

  loginSubmit() {

    const user= {
      aadharno:this.aadharno,
      password:this.password,
      token: this.token,
    }

    this.authService.login(user).subscribe(data=> {
      console.log(data);
      if(data.success){
        this.authService.storeUserData(data.token,data.user);
        this.router.navigate(['/dashboard']); 
      }

      else{
        if(data.error=="invalid_aadharno") {
          alert('Enter valid aadhar number!');
          return false;
        }
        if(data.error=="invalid_password") {
          alert('Enter valid password!');
          return false;
        }
        if(data.error=="invalid_token") {
          alert('Enter valid 2 FA token!');
          return false;
        }
      }
    });
  }

}
